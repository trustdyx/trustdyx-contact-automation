1. Download trustdyx-contact-form-run.py
2. Download Chromedriver (keep a note of the file destination)
3. Create a new virtual environment in the same directory of the script
   a. Activate the virtual environment
4. Install Selenium to our environment: pip install selenium
5. Specify the path to chromedriver.exe in line 8
6. Specify the path to chrome.exe in line 12
7. Run **This will create a submission for every form specified in the script**